using System;
using System.Collections.Generic;
using Xunit;
using QuadraticEquation;

namespace QuadraticEquation.Tests
{
    public class UnitTest1
    {
        const double eps = 1e-5;

        private bool EquationShouldHasRoots (double expected1, double expected2, double[] roots, double eps = 1e-5)
        {
            return Math.Abs(roots[0]-expected1)<eps && Math.Abs(roots[1]-expected2)<eps;
        }
    
        [Fact]
        public void SE_1_Minus2_1_MustHaveTwoDifferentRoots()
        {
            Class1 c = new Class1();

            double[] result = c.solve(1, -2, 1, eps);

            Assert.True(EquationShouldHasRoots(1, 1, result, eps));
        }
        
        [Fact]
        public void SE_1_0_Minus1_MustHaveTwoRoots()
        {
            Class1 c = new Class1();

            double[] result = c.solve(1, 0, -1, eps);

            Assert.True(EquationShouldHasRoots(1, -1, result, eps));
        }

        [Fact]
        public void SE_1_0_1_MustHaveTwoRoots() 
        {
            Class1 c = new Class1();

            double[] result = c.solve(1, 0, 1, eps);

            Assert.Empty(result);
        }

        [Fact]

        public void CannotBeZero() 
        {
            Class1 c = new Class1();

            Assert.Throws<ArgumentException>(()=>c.solve(1e-6, 1, 1));
        }
    }
}
