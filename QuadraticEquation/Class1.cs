﻿using System;
using System.Collections.Generic;

namespace QuadraticEquation
{
    public class Class1
    {
        public double[] solve (double a, double b, double c, double eps = 1e-5)
        {
            if (Math.Abs(a)<eps)
            {
                throw new ArgumentException ("Cannot be zero");
            }

            double d2 = b*b-4*a*c;
            if(d2 >= eps)
            {
                double d = Math.Sqrt(d2);
                return new double[2] {(d-b)/2/a, (-d-b)/2/a};
            }
            else
            {
                if (Math.Abs(d2) <= eps)
                {
                    return new double[2] {-b/2/a, -b/2/a};
                }
            }
            return new double[0];
        }
    }
}
